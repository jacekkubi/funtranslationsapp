﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class TranslationsController : Controller
    {
        private string DefaultConnection = "datasource = localhost; port = 3306; user=admin; password = admin; database = funtranslation; SslMode=none";
        private FunTranslationContext Context;
        string after;
        // GET: Translations
        public ActionResult Index(String Text_after)
        {
            ViewBag.Message = Text_after;
           /*
            Text_after = this.HttpContext.Request.FilePath;
            for(int i = 0; i < 20; i++)
            {
                after += Text_after[0];
            }
            after += "?Text_after=";
            Text_after = after;
            //TextAfter.Remove(20, TextAfter.Count());
            */
            return View();
        }

        // GET: Translations/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Translations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Translations/Create
        [HttpPost]
        public ActionResult Create(String TextBefore)
        {
            Translations Trans = new Translations(); 
            Trans.Text_before = TextBefore;
            Trans.Id_translation = 1;
            //try
            //{
                Context = new FunTranslationContext(DefaultConnection);
                Trans.Text_after = Context.LeetspeakTranslator(TextBefore);

            return Index(Trans.Text_after);

               // return RedirectToAction("Index", new { id = Trans.Text_after });
            //}
           // catch
            //{
            //    return View(Trans.Text_before);
            //}
        }

        // GET: Translations/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Translations/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Translations/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Translations/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
