﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;

namespace WebApplication6.Models
{
    public class FunTranslationContext
    {
        private string ConnectionString { get; set; }
        private MySqlDataAdapter DataAdapter;
        private DataSet DataSet;
        private List<Languages> ListOfLanguages;
        private string TextAfter;

        public FunTranslationContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public List<Languages> GetListOfAllLanguages()
        {
            ListOfLanguages = new List<Languages>();
            try
            {
                using(MySqlConnection Connection = GetConnection())
                {
                    Connection.Open();
                    DataSet = new DataSet();
                    DataAdapter = new MySqlDataAdapter("SELECT * FROM languages",Connection);
                    Connection.Close();
                    DataAdapter.Fill(DataSet);
                    foreach(DataRow dr in DataSet.Tables[0].Rows)
                    {
                        ListOfLanguages.Add(new Languages { Id_language = int.Parse(dr[0].ToString()), Name_language = dr[1].ToString()});
                    }
                }
            }
            catch(Exception e)
            {
                throw (e);
            }
            return ListOfLanguages;
        }

        public string LeetspeakTranslator(string TextBefore)
        {
            LeetspeakAPI Object = new LeetspeakAPI();
            LeetspeakAPI.Contents ObjectAfter;
            ObjectAfter = Object.ChangeText(TextBefore);
            TextAfter = ObjectAfter.translated;
            try
            {
                using (MySqlConnection con = GetConnection())
                {
                    con.Open();
                    MySqlCommand comm = con.CreateCommand();
                    comm.CommandText = "INSERT INTO translations(id_translation,text_before,text_after,id_language) VALUES(@first, @second, @third, @fourth)";
                    comm.Parameters.AddWithValue("@first", "");
                    comm.Parameters.AddWithValue("@second", TextBefore);
                    comm.Parameters.AddWithValue("@third", TextAfter);
                    comm.Parameters.AddWithValue("@fourth", "1");
                    comm.ExecuteNonQuery();
                    con.Close();
                }

            }
            catch(Exception e)
            {
                throw (e);
            }
            return TextAfter;
        }

    }
}