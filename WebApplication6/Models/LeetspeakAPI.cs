﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using static WebApplication6.Models.LeetspeakAPI;

namespace WebApplication6.Models
{
    public class LeetspeakAPI
    {
        public class Success
        {
            public int total { get; set; }
        }

        public class Contents
        {
            public string translated { get; set; }
            public string text { get; set; }
            public string translation { get; set; }
        }

        public class Error
        {
            public int code { get; set; }
            public string message { get; set; }
        }

        public class RootObject
        {
            public Success success { get; set; }
            public Contents contents { get; set; }
            public Error error { get; set; }
        }

        public Contents ChangeText(string TextBefore)
        {
            RootObject Object = new RootObject();
            Object.contents = new RootObject().contents;
            var url = "http://api.funtranslations.com/translate/leetspeak.json?text=" + TextBefore;
            var wc = new WebClient();
            var JSon = wc.DownloadString(url);
            Object = JsonConvert.DeserializeObject<RootObject>(JSon);
            return Object.contents;
        }

    }
}