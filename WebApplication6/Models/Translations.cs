﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class Translations
    {
        public int Id_translation { get; set; }
        public string Text_before { get; set; }
        public string Text_after { get; set; }
        public int Id_language { get; set; }
    }
}