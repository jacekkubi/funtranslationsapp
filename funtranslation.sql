-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 04 Cze 2018, 22:42
-- Wersja serwera: 10.1.30-MariaDB
-- Wersja PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `funtranslation`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `languages`
--

CREATE TABLE `languages` (
  `id_language` int(11) NOT NULL,
  `name_language` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `languages`
--

INSERT INTO `languages` (`id_language`, `name_language`) VALUES
(1, 'Numbers');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `translations`
--

CREATE TABLE `translations` (
  `id_translation` int(11) NOT NULL,
  `text_before` varchar(500) NOT NULL,
  `text_after` varchar(500) NOT NULL,
  `id_language` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `translations`
--

INSERT INTO `translations` (`id_translation`, `text_before`, `text_after`, `id_language`) VALUES
(1, 'Write here your text', 'wr!7e ]-[erE j00R TeXt', 1),
(2, 'Write here your text', 'wr|73 ]-[er3 j00R tEkzT', 1),
(3, 'Write here your text', 'WRit3 H3rE j00R 7ekzt', 1),
(4, 'Write here your text', 'Wr1t3 h3RE j00r 7Ext', 1),
(5, 'Write here your text', 'wR!t3 h3r3 j00r t3XT', 1),
(6, 'Write here your text', '\\/\\/rit3 H3RE j00R tExT', 1),
(7, 'Write here your text', 'wR!73 h3R3 j00R t3xt', 1),
(8, 'Write here your text', 'Too many requests', 1),
(9, 'Write here your text', 'N/A', 1),
(10, 'Write here your text', 'wr17E heR3 j00R 73x7', 1),
(11, 'Write here your text', 'WrIte HER3 j00r T3kz7', 1),
(12, 'Jacek Kubiak lubi jesc slonine', ';4CEC kubiAc luB| J35C Sl0n!n3', 1),
(13, 'Jakub Zielinski', 'j@KUB Zi3l!NSKY', 1),
(14, 'Sagan lubi w dupke', 'zag4N LUb1 w dUpc3', 1),
(15, 'Jakub Zielinski', ';4Cu8 s13lYNzc1', 1),
(16, 'Jacek Kubiak lubi jesc slonine', 'j4c3c Kub|@k lU8! jeSc SlO]\\[|nE', 1),
(17, 'Write here your text', 'WRY73 hEre j00r T3kzT', 1),
(18, 'Write here your text', 'WRIt3 h3RE j00r teXT', 1),
(19, 'Write here your text', 'wRiTE hER3 j00R Tekz7', 1),
(20, 'Write here your text', 'WRYte h3R3 j00r 7Ekz7', 1),
(21, 'Write here your text', 'wrI73 h3r3 j00r 73kz7', 1),
(22, 'Write here your text', 'Writ3 h3re j00R tekzt', 1);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id_language`);

--
-- Indeksy dla tabeli `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id_translation`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `languages`
--
ALTER TABLE `languages`
  MODIFY `id_language` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `translations`
--
ALTER TABLE `translations`
  MODIFY `id_translation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
